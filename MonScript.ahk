; *************************************************************************************************
; To make this script more user-friendly
; use sublimetext2 with YAML syntax highlighting
; *************************************************************************************************

; # = Window Key
; ! = Alt
; ^ = Ctrl
; + = Shift
; https://www.autohotkey.com/docs/Hotkeys.htm
; ===================================================================================================
; Retrieve the ID/HWND of the active window
; https://www.autohotkey.com/docs/misc/WinTitle.htm
^!M::
	id := WinExist("A")
	MsgBox % id
return

; https://www.autohotkey.com/docs/commands/WinGetClass.htm
^!N::
	WinGetClass, class, A
	MsgBox, The active window's class is "%class%".
return

; https://www.autohotkey.com/docs/commands/WinGetTitle.htm
^!B::
	WinGetTitle, Title, A
	MsgBox, The active window is "%Title%".
return

; ====================================================================================================
; Ouvrir le script AHK avec Notepad
/*
^+A::
	; Send, & 'C:\Program Files\AutoHotkey\AutoHotkey.exe' C:\AHK\MonScript.ahk
	Run, C:\Windows\System32\notepad.exe C:\AHK\MonScript.ahk
return
*/
; Ouvrir mon Chrome Bookmark for ANIME
^!A::
	MsgBox, "Opening Chrome Bookmark for ANIME"
	Send, chrome://bookmarks/?id=41
return


; ====================================================================================================
^+B::
	Run, C:\Users\Emanresu\AppData\Local\BraveSoftware\Brave-Browser\Application\brave.exe
return


; ====================================================================================================
; https://www.autohotkey.com/docs/misc/WinTitle.htm
^+C::	
	if WinExist("ahk_exe chrome.exe") {
		msgbox, "CHROME IF"
		WinActivate ; uses the last found window.
	} else if WinExist("Messenger - Google Chrome") {
		msgbox, "CHROME IF"
		WinActivate ; uses the last found window.
	} else {
		msgbox, "CHROME ELSE"
		Run, "C:\Program Files\Google\Chrome\Application\chrome.exe"
	}
return

; ====================================================================================================
^+X::
	; Run, C:\Program Files (x86)\Google\Chrome\Application\chrome.exe --incognito https://www.proxysite.com/
	; Run, C:\Program Files (x86)\Google\Chrome\Application\chrome.exe --incognito https://torrentz2.eu/
	; Run, C:\Program Files (x86)\Google\Chrome\Application\chrome.exe --incognito https://torrentz2.is
	; Run, C:\Program Files\Google\Chrome\Application\chrome.exe --incognito https://torrentz2.is
	Run, C:\Program Files\Google\Chrome\Application\chrome.exe --incognito
return


; ====================================================================================================
; Hibernate computer
^+H::
	if WinExist("ahk_exe powershell.exe") {
		WinActivate ; uses the last found window.
		Send, shutdown /f /h
	}
return


; ====================================================================================================
^+P::
	if WinExist("ahk_exe powershell.exe") {
		WinActivate ; uses the last found window.
		Send, Stop-Process -Force -Name **
	} else {
		; Run, C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe Start-Process 'C:\Program Files (x86)\Google\Chrome\Application\chrome.exe' -verb RunAs
		Run, C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe Start-Process 'C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe' -verb RunAs
	}
return


; ====================================================================================================
^+V::
	Run, C:\Program Files\VideoLAN\VLC\vlc.exe
return


; ====================================================================================================
^+Y::
	Run, C:\Program Files\VideoLAN\VLC\vlc.exe "C:\Users\Emanresu\Music\2019_11_10_Shape of You - Ed Sheeran (Koto cover).wav"
return


^+T::
	MsgBox "Opening Notepad"
	Run, "C:\Windows\System32\notepad.exe"
return

; =================================================================================================
; ============================== GITLAB LABELS ====================================================
; =================================================================================================
; https://www.autohotkey.com/docs/misc/Clipboard.htm
^!1::
	; BLACK
	Send, BACKLOG
	clipboard := "#000000"
return
^!2::
	; GRAY
	Send, TODO
	clipboard := "#808080"
return
^!3::
	; ORANGE
	Send, DOING
	clipboard := "#ffa500"
return
^!4::
	; YELLOW
	Send, CODE REVIEW
	clipboard := "#ffff00"
return
^!5::
	; VIOLET
	Send, TEST
	clipboard := "#ee82ee"
return
^!6::
	; GREEN
	Send, DONE
	clipboard := "#00ff00"
return
; ============================== TYPES OF TODO
^!7::
	; RED
	Send, BUG
	clipboard := "#ff0000"
return
^!8::
	; BLUE
	Send, FEATURE
	clipboard := "#0000ff"
return
^!9::
	; BROWN
	Send, DOCUMENTATION
	clipboard := "#a5682a"
return
^!0::
	; PINK
	Send, IMPROVEMENT
	clipboard := "#ffc0cb"
return
^!Numpad1::
	; CYAN
	Send, <Placeholder>
	clipboard := "#00ffff"
return
^!Numpad2::
	; MAGENTA
	Send, <Placeholder>
	clipboard := "#ff00ff"
return

; =================================================================================================
; =================================================================================================
; ============================== H O T S T R I N G S ==============================================
; =================================================================================================
; =================================================================================================
::AP::
	Send, AP65550
return
::spfn::
	Send, stop-process -Force -Name **
return
::sss::
	Send, shutdown /s /f /t 0
return
::rs::
	Send, & "C:\Program Files\AutoHotkey\AutoHotkey.exe" "C:\Users\emanresu\Desktop\AHK\MonScript.ahk"
return
::pl::
	Send, PLURALSIGHT_
return
::ud::
	Send, UDEMY_
return
::ly::
	Send, LYNDA_
return

; =================================================================================================
; ============================== GIT-RELATED ======================================================
; OFFICIAL DOCS
; https://git-scm.com/docs
; Gitlab SSH Key doc
; https://docs.gitlab.com/ee/ssh/README.html#adding-an-ssh-key-to-your-gitlab-account
; Version Control System (vcs) VS Source Control Management (SCM)
; https://stackoverflow.com/questions/4127425/whats-the-difference-between-vcs-and-scm
; =================================================================================================
::ga::git add -u
::gb::git branch
::gc::git checkout
::gd::git diff --staged
::gi::git init
::gl::git log --oneline -n
::gm::git commit -m 
; ================= https://stackoverflow.com/questions/16217363/git-graph-not-showing-branch
::gmm::git merge --no-ff 
::gp:: git pull
::gpp::git push
; ================= git remove command documentation (https://git-scm.com/docs/git-remote)
; ::gr::git restore
::gr::git remote --verbose
::gra::git remote add
::grr::git remote remove
; ================= file status
::gs::git status
; ================= https://git-scm.com/docs/git-merge (https://stackoverflow.com/questions/15006554/git-merge-branch-and-use-meaningful-merge-commit-message)
; git merge <source-branch>
; git merge <source-branch> -m "Merge 'source-branch' to 'destination-branch'"
; ================= https://git-scm.com/docs/git-rm
; git rm
; ============================== GIT CONFIG (FIRST-TIME USER) ==============================
; git config --global user.name "your-username"
; git config --global user.email "your-email-address@example.com"
; ================= Git DiffTools (https://stackoverflow.com/questions/33308482/git-how-configure-kdiff3-as-merge-tool-and-diff-tool)
; git config --global merge.tool kdiff3
; git config --global mergetool.kdiff3.path "C:\Program Files\KDiff3\kdiff3.exe"
; git config --global mergetool.kdiff3.trustExitCode false
; git config --global diff.guitool kdiff3
; git config --global difftool.kdiff3.path "C:\Program Files\KDiff3\kdiff3.exe"
; git config --global difftool.kdiff3.trustExitCode false
; ================= TO LIST YOUR LOCAL CONFIG
; git config --list

; =================================================================================================
; ============================== JUNIT-RELATED ====================================================
; =================================================================================================
::aaa::// Arrange{enter}{enter}// Act{enter}{enter}// Assert{enter}

; =================================================================================================
; ============================== FIREBASE-RELATED =================================================
; =================================================================================================
::fd::firebase deploy

; =================================================================================================
; ============================== NUXTJS-JS-RELATED ================================================
; =================================================================================================
::rg::npm run build && npm run generate

; =================================================================================================
; ============================== POWERSHELL =======================================================
; =================================================================================================
; ================= How to create alias (https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/set-alias?view=powershell-7.1)
; Set-Alias -Name subl -Value 'C:\Program Files\Sublime Text 2\sublime_text.exe'
; Get-Alias -Name subl

; =================================================================================================
; ============================== CHOCOLATEY-RELATED ===============================================
; =================================================================================================
::cif::choco install --yes --force
::cu::choco upgrade all --yes
::cl::choco list --local

; =================================================================================================
; ============================== NUANCE-RELATED ===================================================
; =================================================================================================
::hn::Hi Nirbhay
::bns::C:\Users\emanresu\Desktop\BNS_SPAR\WebContent\dialogs\application

; =================================================================================================
; ============================== EMAIL-RELATED ====================================================
; =================================================================================================
::ssg::samnangsuon.ss@gmail.com
::ssn::samnang.suon@nuance.com

; =================================================================================================
; ============================== MY-CHOCOLATEY-PACKAGES ===========================================
; =================================================================================================
; ========== I PREFER ADOBE PREMIERE PRO
; choco install --yes --force kdenlive
; choco install --yes --force openshot
; choco install --yes --force shotcut.install


; ========== I PREFER CAMTASIA
; https://www.freescreenrecording.com/


; ========== THE FOLLOWING ARE PAID SOFTWARE
; choco install --yes --force groupy
; choco install --yes --force stardock-fences


; ========== THE FOLLOWING ARE OPEN SOURCE AND FREE SOFTWARE
; choco install --yes --force 7zip
; choco install --yes --force 7zip.install
; choco install --yes --force ant
; choco install --yes --force audacity
; choco install --yes --force autohotkey
; choco install --yes --force autohotkey.install
; choco install --yes --force bleachbit
; choco install --yes --force bleachbit.install
; choco install --yes --force brave
; choco install --yes --force ccleaner
; choco install --yes --force chocolateyGUI
; choco install --yes --force cpu-z
; choco install --yes --force cpu-z.install
; choco install --yes --force curl
; choco install --yes --force defraggler
; choco install --yes --force DotNet4.5
; choco install --yes --force foxitreader
; choco install --yes --force gimp
; choco install --yes --force git
; choco install --yes --force git.install
; choco install --yes --force github-desktop
; choco install --yes --force GoogleChrome
; choco install --yes --force gradle
; choco install --yes --force greenshot
; choco install --yes --force intellijidea-ultimate
; choco install --yes --force jfrog-cli
; choco install --yes --force kdiff3
; choco install --yes --force keepass
; choco install --yes --force keepass.install
; choco install --yes --force mpc-hc
; choco install --yes --force nodejs
; choco install --yes --force nodejs.install
; choco install --yes --force notepadplusplus
; choco install --yes --force notepadplusplus.install
; choco install --yes --force pdfsam
; choco install --yes --force postman
; choco install --yes --force python
; choco install --yes --force python3
; choco install --yes --force putty
; choco install --yes --force qbittorrent
; choco install --yes --force qdir
; choco install --yes --force revo-uninstaller
; choco install --yes --force rufus
; choco install --yes --force sandboxie
; choco install --yes --force sourcetree
; choco install --yes --force speccy
; choco install --yes --force teracopy
; choco install --yes --force treesizefree
; choco install --yes --force ultradefrag
; choco install --yes --force unchecky
; choco install --yes --force vcredist-all
; choco install --yes --force vlc
; choco install --yes --force vscode
; choco install --yes --force wireshark
; choco install --yes --force zoom
